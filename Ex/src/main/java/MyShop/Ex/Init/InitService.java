package MyShop.Ex.Init;


import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.service.ItemService;
import MyShop.Ex.service.MemberService;
import MyShop.Ex.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * PostConstruct를 쓰면 Transactional 포함 모든걸 다 넣으면 동작 안할 가능성 있다.
 * 빈마다 라이프사이클이 있기때문. => 별도의 빈으로 등록해야함.
 */

//    @PostConstruct
@Component
@Transactional
@RequiredArgsConstructor
public class InitService {

    private final MemberService memberService;
    private final ItemService itemService;
    private final OrderService orderService;

    private List<Member> members = new ArrayList<>();
    private List<Item> items = new ArrayList<>();

    public void initMember(){
        for(int i=0;i<=10;i++){
            Member member = Member.createMember("kim"+i, i+"asd@naver.com");
            member.changeAddress(new Address(i+"",i+"",i+""));
            members.add(member);
            memberService.join(member);
        }
    }

    public void initItems(){
        for(int i=0;i<=10;i++){
            Item item = Item.createItem("book"+i, 1000+i*1000, 5000);
            items.add(item);
            itemService.saveItem(item);
        }
    }

    public void initOrders(){

        for(int i=1;i<items.size();i++){
            Item item1  = items.get((int)(Math.random()*items.size()));
            Item item2  = items.get((int)(Math.random()*items.size()));
            Member member = members.get((int)(Math.random()*members.size()));

            HashMap<Long, Integer> itemMap = new HashMap<>();
            itemMap.put(item1.getId(),i);
            itemMap.put(item2.getId(),i);

            orderService.order(member.getId(),itemMap);
        }
    }
}