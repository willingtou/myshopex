package MyShop.Ex.Init;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.service.ItemService;
import MyShop.Ex.service.MemberService;
import MyShop.Ex.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@Component
@RequiredArgsConstructor
public class InitDb {
    private final InitService initService;

    @PostConstruct
    public void init(){
        initService.initItems();
        initService.initMember();
        initService.initOrders();
    }
}

