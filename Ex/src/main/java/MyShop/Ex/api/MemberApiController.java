package MyShop.Ex.api;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.service.MemberService;
import MyShop.Ex.web.form.MemberForm;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class MemberApiController {

    private final MemberService memberService;

    /**
     * 회원가입
     */
    @PostMapping("/members")
    public CreateMemberResponse create(@RequestBody @Valid CreateMemberRequest request){
        Member member = Member.createMember(request.getName(), request.getEmail());
;
        member.changeAddress(request.getAddress());
        Long id = memberService.join(member);

        return new CreateMemberResponse(id);
    }

    @Data
    private static class CreateMemberRequest {
        private String name;
        private String email;
        private Address address;
    }

    @Data
    @AllArgsConstructor
    private static class CreateMemberResponse {
        private Long id;
    }

    /**
     * 수정 API
     * 추후 회원에 대해 검증을 할것 (악의적 id값 변조 방지)
     */

    @PatchMapping("/members/{id}")
    public UpdateMemberResponse updateMember(@PathVariable("id") Long id,
                                             @RequestBody @Valid UpdateMemberRequest request){

        memberService.updateMember(new MemberForm(id, request.getName(), request.getEmail(), request.getAddress()));
        Member findMember = memberService.findOne(id).get();
        return new UpdateMemberResponse(findMember.getId(), findMember.getName(), findMember.getEmail(), findMember.getAddress());
    }

    @Data
    private static class UpdateMemberRequest {
        private String name;
        private String email;
        private Address address;
    }

    @Data
    @AllArgsConstructor
    private static class UpdateMemberResponse {
        private Long id;
        private String name;
        private String email;
        private Address address;
    }


    /**
     * 회원조회 API
     */
    @GetMapping("/members")
    public Result members(){
        return new Result(memberService.findMembers().stream()
                .map(m->new MemberDto(m.getName(),m.getEmail(),m.getAddress()))
                .collect(Collectors.toList()));
    }


    @Data
    @AllArgsConstructor
    private static class MemberDto{
        private String name;
        private String email;
        private Address address;
    }
}
