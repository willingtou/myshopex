package MyShop.Ex.api;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.order.Order;
import MyShop.Ex.domain.order.OrderRepository;
import MyShop.Ex.domain.order.OrderStatus;
import MyShop.Ex.domain.orderitem.OrderItem;
import MyShop.Ex.service.OrderService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Order
 * Order -> Member (Many to One)
 * Order -> Delivery (One to One)
 *
 * DTO로 바로 뽑아내는 방법이 있으나 , 일단 보류.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class OrderApiController {

    private final OrderService orderService;

    @GetMapping("/orders")
    public Result orders(@RequestParam(value="offset",defaultValue = "0") int offset,
                         @RequestParam(value="limit",defaultValue = "100") int limit){

        return new Result(orderService.findAll(offset,limit).stream()
                .map(OrderDto::new)
                .collect(Collectors.toList()));
    }

    /**
     * 유저 이름으로 단건조회
     */
    @GetMapping("/orders/order")
    public Result findOrderByName(@RequestParam String name){
        return new Result(orderService.findOrderByName(name));
    }

    /**
     * 최적화가 적용된 전체조회 (추후 페이징과 복잡한것들 추가 예정 )
     */
    @GetMapping("/v2/orders")
    public Result findOrderWithOptimization(){
        return new Result(orderService.findOrdersWithOptimization());
    }

    @Data
    @AllArgsConstructor
    private static class OrderDto{
        private Long orderId;
        private String name;
        private LocalDateTime orderDate; //주문시간
        private OrderStatus orderStatus;
        private Address address;
        private List<OrderItemDto> orderItems;

        public OrderDto(Order order) {
            orderId = order.getId();
            name = order.getMember().getName();
            orderDate = order.getOrderDate();
            orderStatus = order.getStatus();
            address = order.getDelivery().getAddress();
            orderItems = order.getOrderItems().stream()
                    .map(OrderItemDto::new)
                    .collect(Collectors.toList());
        }
    }

    @Data
    private static class OrderItemDto {

        private String name;
        private int orderPrice;
        private int count;

        public OrderItemDto(OrderItem orderItem) {
            this.name = orderItem.getItem().getName();
            this.orderPrice = orderItem.getOrderPrice();
            this.count = orderItem.getCount();
        }
    }
}
