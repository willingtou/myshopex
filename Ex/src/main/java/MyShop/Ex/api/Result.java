package MyShop.Ex.api;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * result 로 감싸는걸 필수로. (유지보수의 유연성을 위한)
 */
@Data
@AllArgsConstructor
public class Result<T> {
    // 항후 필요하면 다른 필드 추가도 가능. ex) private int count 등
    private T data;
}