package MyShop.Ex;


import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.UUID;

@EnableSwagger2
@SpringBootApplication
@EnableJpaAuditing
public class ExApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExApplication.class, args);
	}


	// 등록자 수정자 처리 auditing (추후 추가예정)
	@Bean
	public AuditorAware<String> auditorProvider(){
		return ()-> Optional.ofNullable(Optional.of(UUID.randomUUID()).toString());
	}

	@Bean
	Hibernate5Module hibernate5Module() {
		//		hibernate5Module.configure(Hibernate5Module.Feature.FORCE_LAZY_LOADING, true);
		return new Hibernate5Module();
	}

	@Bean
	JPAQueryFactory jpaQueryFactory(EntityManager em){
		return new JPAQueryFactory(em);
	}
}
