package MyShop.Ex.web.form;

import MyShop.Ex.domain.item.Item;
import lombok.Data;

@Data
public class ItemForm {

    private Long id;

    private String name;
    private int price;
    private int stockQuantity;

}
