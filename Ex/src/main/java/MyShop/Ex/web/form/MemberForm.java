package MyShop.Ex.web.form;

import MyShop.Ex.domain.address.Address;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
public class MemberForm {

    private Long id;

    @NotEmpty(message = "필수 항목입니다")
    private String name;

    @NotEmpty(message = "필수 항목입니다.")
    private String email;

    private String city;
    private String street;
    private String zipcode;

    public MemberForm(Long id,String name, String email, Address address){
        this.id=id;
        this.name=name;
        this.email=email;
        this.city = address.getCity();
        this.street = address.getStreet();
        this.zipcode = address.getZipcode();
    }

}
