package MyShop.Ex.web.controller;

import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.domain.order.Order;
import MyShop.Ex.domain.order.OrderSearch;
import MyShop.Ex.service.ItemService;
import MyShop.Ex.service.MemberService;
import MyShop.Ex.service.OrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RequestMapping("/order")
@RequiredArgsConstructor
@Controller
@Slf4j
public class OrderController {

    private final OrderService orderService;
    private final MemberService memberService;
    private final ItemService itemService;

    @GetMapping("")
    public String createForm(Model model) {
        List<Member> members = memberService.findMembers();
        List<Item> items = itemService.findItems();

        model.addAttribute("members", members);
        model.addAttribute("items", items);

        return "order/orderForm";
    }

    @PostMapping("")
    public String order(Long memberId, Long itemId, Integer count) {
        HashMap<Long, Integer> itemMap = new HashMap<>();
        itemMap.put(itemId, count);
        orderService.order(memberId, itemMap);
        return "redirect:/order/orders";
    }


    @GetMapping("/orders")
    public String orderList(OrderSearch orderSearch, Model model) {

        List<Order> orders = orderService.findAll(); // 일단 전체조회
        //        log.info("orders = {}",orders.toString());
        model.addAttribute("orders", orders);

        return "order/orderList";
    }


    @PostMapping("/orders/{orderId}/cancel")
    public String cancelOrder(@PathVariable("orderId") Long orderId) {
        log.info("orderId = {}", orderId);
        orderService.cancelOrder(orderId);
        return "redirect:/order/orders";
    }
}