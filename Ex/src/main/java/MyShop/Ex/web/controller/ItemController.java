package MyShop.Ex.web.controller;

import MyShop.Ex.domain.item.Item;
import MyShop.Ex.service.ItemService;
import MyShop.Ex.web.form.ItemForm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
@RequestMapping("/items")
public class ItemController {

    private final ItemService itemService;

    /**
     * 상품 등록 폼 요청
     */
    @GetMapping("/new")
    public String createForm(Model model){

        model.addAttribute("form",new ItemForm());
        return "items/createItemForm";
    }

    /**
     * 상품 등록
     */
    @PostMapping("/new")
    public String create(ItemForm form){
        Item item = Item.createItem(form.getName(), form.getPrice(), form.getStockQuantity());

        itemService.saveItem(item);

        return "redirect:/items";
    }

    /**
     * 상품 조회
     */
    @GetMapping("")
    public String list(Model model){

        List<Item> items = itemService.findItems();

        model.addAttribute("items",items);
        return "items/itemList";
    }

    /**
     * 상품 수정 폼
     */
    @GetMapping("/{itemId}/edit")
    public String updateItemForm(@PathVariable("itemId") Long itemId,Model model){

        Item findItem = itemService.findOne(itemId);
        ItemForm form = new ItemForm();

        form.setId(findItem.getId());
        form.setName(findItem.getName());
        form.setPrice(findItem.getPrice());
        form.setStockQuantity(findItem.getStockQuantity());

        model.addAttribute("form",form);
        return "items/updateItemForm";
    }

    /**
     * 상품 수정
     */
    @PostMapping("/{itemId}/edit")
    public String updateItem(@ModelAttribute("form") ItemForm form) {

        itemService.updateItem(form);
        return "redirect:/items";
    }

    /**
     * 상품 할인 (임시 기능)
     * 추후 권한 추가해서 할인기능을 분리할 것.
     */
    @GetMapping("/discount")
    public String discountItems(){
        List<Long> list = itemService.findItems().stream().map(Item::getId).collect(Collectors.toList());
        itemService.discountItems(80, list);

        return "redirect:/items";
    }
}
