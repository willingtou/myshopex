package MyShop.Ex.web.controller;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.service.MemberService;
import MyShop.Ex.web.form.MemberForm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
@RequestMapping("/members")
public class MemberController {

    private final MemberService memberService;

    /**
     * 회원 가입 폼 요청
     */
    @GetMapping("/new")
    public String createForm(Model model){

        model.addAttribute("memberForm",new MemberForm());
        return "members/createMemberForm";
    }

    /**
     * 회원 등록
     */
    @PostMapping("/new")
    public String create(@Valid MemberForm form, BindingResult result){

        if(result.hasErrors())
            return "members/createMemberForm";

        Member member = Member.createMember(form.getName(), form.getEmail());
        member.changeAddress(
                new Address(form.getCity(),form.getStreet(),form.getZipcode()));
        memberService.join(member);

        return "redirect:/";
    }

    /**
     * 회원 조회
     */
    @GetMapping("")
    public String list(Model model){
        List<Member> members = memberService.findMembers();
        model.addAttribute("members",members);
        return "members/memberList";
    }

}
