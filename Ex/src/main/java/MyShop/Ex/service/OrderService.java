package MyShop.Ex.service;

import MyShop.Ex.domain.delivery.Delivery;
import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.domain.order.Order;
import MyShop.Ex.domain.order.OrderRepository;
import MyShop.Ex.domain.order.query.OrderQueryDto;
import MyShop.Ex.domain.orderitem.OrderItem;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
public class OrderService {

    //service
    private final MemberService memberService;
    private final ItemService itemService;

    //repository
    private final OrderRepository orderRepository;

    /**
     * 주문
     */
    @Transactional
    public Long order(Long memberId, HashMap<Long,Integer> itemMap) {

        //엔티티 조회
        Member member = memberService.findOne(memberId).get();

        //배송정보 생성
        Delivery delivery = Delivery.createDeliveryInfo(member);

        // Map 으로 부터 받은 상품 정보 조회
        List<Item> items = itemService.findItemsByIds(new ArrayList<>(itemMap.keySet()));

        // 여러 주문 아이템들을 주문상품 리스트로 변환
        List<OrderItem> orderItemList = new ArrayList<>();
        items.forEach(item-> orderItemList.add(OrderItem.createOrderItem(item,item.getPrice(),itemMap.get(item.getId()))));


        //주문 생성
        Order order = Order.createOrder(member, delivery, orderItemList);

        //주문 저장
        return orderRepository.save(order).getId();
    }

    /**
     * 주문 취소
     */
    @Transactional
    public void cancelOrder(Long orderId) {
        //주문 엔티티 조회
        Optional<Order> order = orderRepository.findById(orderId);
        //주문 취소
        order.get().cancel();
    }

    /**
     * 주문조회 (단건)
     */
    @Transactional(readOnly = true)
    public Optional<Order> findOne(Long orderId){
        return orderRepository.findById(orderId);
    }

    /**
     * 주문조회 전체
     */
    // 페이징 o
    @Transactional(readOnly = true)
    public Page<Order> findAll(int offset, int limit){
        PageRequest pageRequest = PageRequest.of(offset, limit);
        return orderRepository.findAll(pageRequest);
    }


    @Transactional(readOnly = true)
    public List<Order> findAll(){
        return orderRepository.findAll();
    }

    // 페이징 x , 단건 조회
    @Transactional(readOnly = true)
    public List<OrderQueryDto> findOrderByName(String name){
        return orderRepository.findOrderByName(name);
    }

    // 페이징 x ( MemoryMap 최적화 적용 )
    @Transactional(readOnly = true)
    public List<OrderQueryDto> findOrdersWithOptimization(){
        return orderRepository.findAllByDtoWithOptimization();
    }


}
