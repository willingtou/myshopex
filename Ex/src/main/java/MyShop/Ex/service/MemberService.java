package MyShop.Ex.service;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.domain.member.MemberRepository;
import MyShop.Ex.web.form.ItemForm;
import MyShop.Ex.web.form.MemberForm;
import lombok.RequiredArgsConstructor;
import org.hibernate.metamodel.model.domain.internal.MapMember;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class MemberService {

    private final MemberRepository memberRepository;

    /**
     * 회원가입
     */
    @Transactional
    public Long join(Member member){

        if(isDuplicate(member)){
            throw new IllegalStateException("이미 존재하는 이메일");
        }

        return memberRepository.save(member)
                .getId();
    }

    /**
     *  회원 조회
     */
    public List<Member> findMembers(){
        return memberRepository.findAll();
    }
    public Optional<Member> findOne(Long memberId){
        return memberRepository.findById(memberId);
    }


    private boolean isDuplicate(Member member) {

        return !memberRepository
                .findMemberByEmail(member.getEmail())
                .isEmpty();
    }

    /**
     * 회원 수정
     */
    @Transactional
    public Long updateMember(MemberForm memberParam){
        Member member = memberRepository.findById(memberParam.getId()).get();
        member.changeAddress(new Address(memberParam.getCity(),memberParam.getStreet(), memberParam.getZipcode()));
        member.updateMember(memberParam.getName(),memberParam.getEmail());

        return member.getId();
    }
}
