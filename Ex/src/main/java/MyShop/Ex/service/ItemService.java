package MyShop.Ex.service;

import MyShop.Ex.domain.auditing.TimeEntity;
import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.item.ItemRepository;
import MyShop.Ex.web.form.ItemForm;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
public class ItemService {

    private final ItemRepository itemRepository;

    @Transactional
    public void saveItem(Item item){
        itemRepository.save(item);
    }

    public List<Item> findItems(){
        return itemRepository.findAll();
    }

    public Item findOne(Long itemId) {
        Optional<Item> findItem = itemRepository.findById(itemId);
        return findItem.get();
    }

    @Transactional
    public void updateItem(ItemForm itemParam){
        Item item = itemRepository.findById(itemParam.getId()).get();
        item.change(itemParam.getName(),itemParam.getPrice(),itemParam.getStockQuantity());
    }

    @Transactional
    public void discountItems(int discountRatio,List<Long> itemId){

        itemRepository.findAllById(itemId).forEach(TimeEntity::setModifiedDate);
        itemRepository.discountItems(discountRatio, itemId);
    }

    @Transactional
    public List<Item> findItemsByIds(List<Long> itemId){

        return itemRepository.findAllById(itemId);
    }
}
