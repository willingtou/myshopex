package MyShop.Ex.domain.item;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item,Long> {

    @Modifying(clearAutomatically = true)
    @Query("update Item i set i.price = i.price*:discountRatio/100 where i.id in :idList")
    int discountItems(@Param("discountRatio")int discountRatio, @Param("idList") List<Long> idList);

}
