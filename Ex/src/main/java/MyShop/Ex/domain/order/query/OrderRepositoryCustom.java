package MyShop.Ex.domain.order.query;


import java.util.List;

public interface OrderRepositoryCustom  {

    List<OrderQueryDto> findOrders(String name);
    List<OrderQueryDto> findOrders();

    List<OrderQueryDto> findAllByDtoWithOptimization();

    List<OrderItemQueryDto> findOrderItems(Long orderId);
    List<OrderQueryDto> findOrderByName(String name);
}
