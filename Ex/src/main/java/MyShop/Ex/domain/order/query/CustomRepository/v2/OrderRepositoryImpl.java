package MyShop.Ex.domain.order.query.CustomRepository.v2;

import MyShop.Ex.domain.delivery.QDelivery;
import MyShop.Ex.domain.item.QItem;
import MyShop.Ex.domain.member.QMember;
import MyShop.Ex.domain.order.QOrder;
import MyShop.Ex.domain.order.query.OrderItemQueryDto;
import MyShop.Ex.domain.order.query.OrderQueryDto;
import MyShop.Ex.domain.order.query.OrderRepositoryCustom;
import MyShop.Ex.domain.orderitem.QOrderItem;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static MyShop.Ex.domain.orderitem.QOrderItem.orderItem;
import static com.querydsl.core.types.Projections.constructor;

@Slf4j
@RequiredArgsConstructor
public class OrderRepositoryImpl implements OrderRepositoryCustom {

    private final JPAQueryFactory queryFactory;
//    private final EntityManager entityManager;

    /**
     * 단건 조회에서 많이 쓰는 방식 (orderItems 에 대한 페이징 적용 안한 단건 )
     * 페이징 적용은 나중에.
     */
    @Override
    public List<OrderQueryDto> findOrderByName(String name) {

        List<OrderQueryDto> result = findOrders(name);

        result.forEach(o->{
            List<OrderItemQueryDto> orderItems = findOrderItems(o.getOrderId());
            o.setOrderItems(orderItems);
        });

        return result;
    }


    // 1:1 인것 먼저 projection
    @Override
    public List<OrderQueryDto> findOrders(String name) {

        QOrder o = QOrder.order;
        QMember m = o.member;
        QDelivery d = o.delivery;

        return queryFactory
                .select(constructor(OrderQueryDto.class,
                        o.id,
                        m.name,
                        o.orderDate,
                        o.status,
                        d.address))
                .from(o)
                .join(m).join(d)
                .where(m.name.eq(name))
                .fetch();

    }

    // 1:N 컬렉션 projection
    @Override
    public List<OrderItemQueryDto> findOrderItems(Long orderId) {

        QOrderItem oi = orderItem;
        QItem i = oi.item;

        return queryFactory
                .select(constructor(OrderItemQueryDto.class,
                        oi.order.id,
                        i.name,
                        oi.orderPrice,
                        oi.count))
                .from(oi)
                .join(i)
                .where(oi.order.id.in(orderId))
                .fetch();
    }



    /**
     * orderItem 마다 추가 쿼리 나가는 문제 해결 하기위한
     * 컬렉션 조회 최적화 ( by Memory Map )
     * 페이징 적용은 나중에.
     */
    @Override
    public List<OrderQueryDto> findAllByDtoWithOptimization(){

        // toOne 모두 조회
        List<OrderQueryDto> result = findOrders();

        List<Long> orderIds = result.stream().map(OrderQueryDto::getOrderId)
                .collect(Collectors.toList());

        // orderItem 컬렉션을 Map 한방에 조회
        Map<Long, List<OrderItemQueryDto>> orderItemMap = findOrderItemMap(orderIds);

        // 루푸를 돌면서 컬렉션 추가 (추가 쿼리 실행x)
        result.forEach(o->o.setOrderItems(orderItemMap.get(o.getOrderId())));

        return result;
    }
    // 전부 조회
    @Override
    public List<OrderQueryDto> findOrders() {

        QOrder o = QOrder.order;
        QMember m = o.member;
        QDelivery d = o.delivery;

        return queryFactory
                .select(constructor(OrderQueryDto.class,
                        o.id,
                        m.name,
                        o.orderDate,
                        o.status,
                        d.address))
                .from(o)
                .join(m).join(d)
                .fetch();
    }

    public Map<Long,List<OrderItemQueryDto>> findOrderItemMap(List<Long> orderIds){

        QOrderItem oi = QOrderItem.orderItem;
        QItem i = oi.item;

        List<OrderItemQueryDto> orderItems = queryFactory
                .select(constructor(OrderItemQueryDto.class,
                        oi.order.id,
                        i.name,
                        oi.orderPrice,
                        oi.count))
                .from(oi)
                .join(i)
                .where(oi.order.id.in(orderIds))
                .fetch();

        return orderItems.stream()
                .collect(Collectors.groupingBy(OrderItemQueryDto::getOrderId));
    }
}
