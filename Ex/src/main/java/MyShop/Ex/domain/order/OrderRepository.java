package MyShop.Ex.domain.order;

import MyShop.Ex.domain.order.query.OrderRepositoryCustom;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> , OrderRepositoryCustom {

    /**
     * in 절로 한번에 끌고오지만 , 애플리케이션 차원에서 distinct를 실행하는 것이기 때문에
     * db자체의 중복데이터는 일단 넘어온다 => 트래픽이 2배
     */
    @Override
    @EntityGraph(attributePaths = {"member","delivery"})
    Page<Order> findAll(Pageable pageable);

    @Override
    @EntityGraph(attributePaths = {"member","delivery"})
    List<Order> findAll();

}
