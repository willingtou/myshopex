package MyShop.Ex.domain.order.query.CustomRepository.v1;

import MyShop.Ex.domain.order.query.OrderItemQueryDto;
import MyShop.Ex.domain.order.query.OrderQueryDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class OrderRepositoryImplV1  {

    private final EntityManager em;

    /**
     * 단건 조회에서 많이 쓰는 방식 (orderItems 에 대한 페이징 적용 안한 단건 )
     * 페이징 적용은 나중에.
     */
//    @Override
    public List<OrderQueryDto> findOrderByName(String name) {

        List<OrderQueryDto> result = findOrders(name);

        result.forEach(o->{
            List<OrderItemQueryDto> orderItems = findOrderItems(o.getOrderId());
            o.setOrderItems(orderItems);
        });

        return result;
    }


    // 1:1 인것 먼저 projection
//    @Override
    public List<OrderQueryDto> findOrders(String name) {

        return em.createQuery(
                "select new MyShop.Ex.domain.order.query.OrderQueryDto(o.id, m.name, o.orderDate,o.status, d.address)"+
                        " from Order o" +
                        " join o.member m" +
                        " join o.delivery d" +
                 " where m.name =:name",OrderQueryDto.class)
                .setParameter("name",name)
                .getResultList();
    }

    // 1:N 컬렉션 projection
//    @Override
    public List<OrderItemQueryDto> findOrderItems(Long orderId) {


        return em.createQuery(
                "select new MyShop.Ex.domain.order.query.OrderItemQueryDto(oi.order.id, i.name, oi.orderPrice, oi.count)" +
                        " from OrderItem oi" +
                        " join oi.item i" +
                        " where oi.order.id = : orderId", OrderItemQueryDto.class)
                .setParameter("orderId", orderId)
                .getResultList();

    }



    /**
     * orderItem 마다 추가 쿼리 나가는 문제 해결 하기위한
     * 컬렉션 조회 최적화 ( by Memory Map )
     * 페이징 적용은 나중에.
     */
//    @Override
    public List<OrderQueryDto> findAllByDtoWithOptimization(){

        // toOne 모두 조회
        List<OrderQueryDto> result = findOrders();


        List<Long> orderIds = result.stream().map(OrderQueryDto::getOrderId)
                .collect(Collectors.toList());

        // orderItem 컬렉션을 Map 한방에 조회
        Map<Long, List<OrderItemQueryDto>> orderItemMap = findOrderItemMap(orderIds);

        // 루푸를 돌면서 컬렉션 추가 (추가 쿼리 실행x)
        result.forEach(o->o.setOrderItems(orderItemMap.get(o.getOrderId())));

        return result;
    }
    // 전부 조회 ( queryDsl에서 수정 예정 )
//    @Override
    public List<OrderQueryDto> findOrders() {

        return em.createQuery(
                "select new MyShop.Ex.domain.order.query.OrderQueryDto(o.id, m.name, o.orderDate,o.status, d.address)"+
                        " from Order o" +
                        " join o.member m" +
                        " join o.delivery d " ,OrderQueryDto.class)
                .getResultList();
    }

    public Map<Long,List<OrderItemQueryDto>> findOrderItemMap(List<Long> orderIds){

        List<OrderItemQueryDto> orderItems = em.createQuery(
                " select new MyShop.Ex.domain.order.query.OrderItemQueryDto(oi.order.id, i.name, oi.orderPrice, oi.count)" +
                        " from OrderItem oi " +
                        " join oi.item i" +
                        " where oi.order.id in :orderIds ", OrderItemQueryDto.class)
                .setParameter("orderIds", orderIds)
                .getResultList();

        return orderItems.stream()
                .collect(Collectors.groupingBy(OrderItemQueryDto::getOrderId));
    }

}
