package MyShop.Ex.domain.delivery;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.domain.order.Order;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString(of={"id","address","status"})
public class Delivery {


    @Id @GeneratedValue
    @Column(name = "delivery_id")
    private Long id;

    @OneToOne(mappedBy = "delivery" ,fetch = FetchType.LAZY)
    private Order order;

    @Embedded
    private Address address;


    /*
    enum타입은 @Enumerated애노태이션을 넣어야하고 ,
    enum타입을 넣을떄 ordinal 이랑 String중 ordinal이 default임.
    중간에 다른게생기면 밀리기떄문에 ordinal이 아닌 string 을 써야함.
     */
    @Enumerated(EnumType.STRING)
    private DeliveryStatus status; //READY , COMP

    public static Delivery createDeliveryInfo(Member member){
        Delivery delivery = new Delivery();
        delivery.address = member.getAddress();
        delivery.status = DeliveryStatus.READY;

        return delivery;
    }
    public void setOrder(Order order){
        // 주문생성 시점에만 사용
        this.order=order;
    }
}
