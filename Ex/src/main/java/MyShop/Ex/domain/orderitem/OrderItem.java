package MyShop.Ex.domain.orderitem;

import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.order.Order;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class OrderItem {

    @Id
    @GeneratedValue
    @Column(name = "order_item_id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_id")
    private Item item;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="order_id")
    private Order order;

    private int orderPrice;
    private int count;

    /**
     //다른 스타일의 생성을 막는방법! jpa는 스펙상 protected까지 허용 -> 컴파일 시점에서 알수있음
     protected OrderItem() {
     }
     or
     @NoArgsConstructor(access = AccessLevel.PROTECTED)
     */


    //==생성 매서드==//
    public static OrderItem createOrderItem(Item item,int orderPrice,int count)
    {
        OrderItem orderItem = new OrderItem();
        orderItem.setItem(item);
        //orderPrice를 따로 가져가는이유 -> 할인정책이 있을때 할인이 될 수 있기때문
        orderItem.setOrderPrice(orderPrice);
        orderItem.setCount(count);

        item.removeStock(count);
        return orderItem;
    }

    //== 비즈니스 로직==//
    public void cancel() {
        getItem().addStock(count);
    }

    //== 조회 로직==//
    /**
     * 주문 상품 전체 가격 조회
     */
    public int getTotalPrice() {
        return getOrderPrice()*getCount();
    }
}