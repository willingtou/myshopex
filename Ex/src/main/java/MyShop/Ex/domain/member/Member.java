package MyShop.Ex.domain.member;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.auditing.TimeEntity;
import MyShop.Ex.domain.order.Order;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Builder
@ToString(of = {"id","name","email","address"})
public class Member extends TimeEntity {

    @Id
    @GeneratedValue
    @Column(name="member_id")
    private Long id;
    private String name;
    private String email;
//    private String password;
    @Embedded
    private Address address;
    @JsonIgnore
    @OneToMany(mappedBy = "member")
    private List<Order> orders = new ArrayList<>();

    public static Member createMember(String name,String email){

        return Member.builder()
                .email(email)
                .name(name)
//                .password(password)
                .build();
    }

    public Member changeAddress(Address address){
        this.address = address;
        return this;
    }
    public void updateMember(String name,String email){
        this.email=email;
        this.name=name;
    }

}
