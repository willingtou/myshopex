package MyShop.Ex.service;

import MyShop.Ex.domain.item.Item;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.domain.order.Order;
import MyShop.Ex.domain.order.OrderStatus;
import MyShop.Ex.exception.NotEnoughStockException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@Rollback(value = false)
class OrderServiceTest {

    @Autowired OrderService orderService;
    @Autowired MemberService memberService;
    @Autowired ItemService itemService;


    @Test
    public void 상품주문() throws Exception{
    
        //given
        Member member = Member.createMember("kim", "nicebyy@naver.com");
        Item item = Item.createItem("시골 JPA", 10000, 10);
        int orderCount = 2;
        memberService.join(member);
        itemService.saveItem(item);

        //when
        HashMap<Long, Integer> itemMap = new HashMap<>();
        itemMap.put(item.getId(),orderCount);
        Long orderId = orderService.order(member.getId(),itemMap);


        //then;
        Order getOrder = orderService.findOne(orderId).get();

        assertEquals(OrderStatus.ORDER, getOrder.getStatus(),"상품 주문시 상태는 ORDER");
        assertEquals(1, getOrder.getOrderItems().size(),"주문한 상품 종류 수가 정확해야 한다.");
        assertEquals( 10000 * 2, getOrder.getTotalPrice(),"주문 가격은 가격 * 수량이다.");
        assertEquals(8, item.getStockQuantity(),"주문 수량만큼 재고가 줄어야 한다.");

    }
    
    @Test
    public void 상품주문_재고수량초과() throws Exception{
    
        //given
        Member member = Member.createMember("kim", "nicebyy@naver.com");
        Item item = Item.createItem("시골 JPA", 10000, 10);
        int orderCount = 11;
        memberService.join(member);
        itemService.saveItem(item);

        //when
        HashMap<Long, Integer> itemMap = new HashMap<>();
        itemMap.put(item.getId(),orderCount);

        //then
        Assertions.assertThrows(NotEnoughStockException.class,
                ()->orderService.order(member.getId(), itemMap));
    }

    @Test
    public void 주문취소() throws Exception{

        //given
        Member member = Member.createMember("kim", "nicebyy@naver.com");
        Item item = Item.createItem("시골 JPA", 10000, 10);
        int orderCount = 2;
        memberService.join(member);
        itemService.saveItem(item);

        HashMap<Long, Integer> itemMap = new HashMap<>();
        itemMap.put(item.getId(),orderCount);

        Long orderId = orderService.order(member.getId(), itemMap);

        //when
        orderService.cancelOrder(orderId);

        //then;
        Order getOrder = orderService.findOne(orderId).get();

        assertEquals(OrderStatus.CANCEL, getOrder.getStatus(),"주문 취소시 상태는 CANCEL 이다.");
        assertEquals( 10, item.getStockQuantity(),"주문이 취소된 상품은 그만큼 재고가 증가해야 한다.");
    }
}