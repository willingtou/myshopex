package MyShop.Ex.service;

import MyShop.Ex.domain.address.Address;
import MyShop.Ex.domain.member.Member;
import MyShop.Ex.domain.member.MemberRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@Slf4j
class MemberServiceTest {

    @Autowired
    MemberService memberService;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    EntityManager em;
    @Test
    void 회원가입() throws Exception{

        Member member = Member.createMember("asd", "nicebyy@naver.com");

        Long savedId = memberService.join(member);
        assertEquals(member,memberService.findOne(savedId).get());
    }

    @Test
    void 중복_예외() throws Exception{


        Member member1 = Member.createMember("asd", "nicebyy@naver.com");
        Member member2 = Member.createMember("asd2", "nicebyy2@naver.com");

        memberService.join(member1);

        assertThrows(IllegalStateException.class,()->memberService.join(member2));
    }

    Member member;
    @Test
    void 영속성체크() throws Exception{
        member = em.find(Member.class, this.member.getId());
        log.info("findMember = {}", this.member.toString());
    }

    @BeforeEach
    void createMember() throws Exception{
        member = Member.createMember("asd", "aaaa@naver.com");
        em.persist(member);
        log.info("saved member = {}",member.toString());
        em.flush();
        em.clear();

        // 영속화 시킴.
        member = em.find(Member.class, this.member.getId());
        member.changeAddress(new Address("대구","길1","우편번호1"));
        em.flush();
    }
    

}