## start: 2022-02-20
## end: 

# stack
## front-end : thymeleaf, css, html, javascript
## back-end : springboot, H2db, springDataJpa, QueryDSL

# 2022-03-01
## 회원, 아이템, 주문 서비스 완료 

# 2022-03-02
## 상품할인 기능 추가(임시)
## 추후 일반사용자(등급), 관리자 분리할 것
## 페이징 쿼리, 페이지 관련 추가할 것

# 2022-03-03
## auditing(날짜) 추가, 추후 secutiry 관련 추가하여 사용자 UUID 쓸것

# 2022-03-04
## API Controller 추기
## API를 위한 로직을 엔티티로부터 분리할 것
## PathVariable 의 ID값 노출을 추후 권한관련 추가할 것
## Body 부분 데이터 검증로직이나 예외 관련 추가할 것

# 2022-03-16
## 기존 JPQL => QueryDsl 로 복잡한 조건 쿼리 별도의 QueryRepository로 분리
## Command Query 분리 예정

# 2022-05-01
## 토이프로젝트 종료.. 
## 이유 : 너무 잡다하게 배운걸 대충 억지로 집어넣다보니 이도저도 아니게 되어버림.
## 앞으로의 방향 : 워밍업 했다고 생각하고 다시 차근차근 새로 할 예정